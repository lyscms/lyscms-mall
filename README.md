# lyscms-mall

#### 介绍
小微购物商城，后台管理，商品上架、下架，商品分类（二级分类），订单管理、商品管理

#### 软件架构
采用了最简洁、最流程的SpringBoot作为基础整合框架
MyBatis作为ORM数据库持久化框架,配合TkMapper使用
视图解析器采用了thymeleaf
前段UI框架采用BootStrap4.0.0, 配合Layui UI经典模块化前端框架

#### 安装教程


1. 下载代码
2. maven方式导入项目
3. 配置maven settings文件下载依赖,不同软件配置方式自行百度
4. 执行maven reimport 刷新maven相关依赖下载，等待完成
5. 新建Mysql数据库名称为:lyscms-mall
6. 执行sql目录下数据库脚本
7. 修改application-loc.properties中的数据库连接信息
8. 执行maven插件:mvn spring-boot:run,也可软件工具方式启动或者执行vrps-sply子模块com.ly.vrps包下的VrpsSpLyApplication.main方法
9. 访问http://localhost


#### 使用说明

1.  前台默认账号密码：lyscms   123456
2.  后台地址：http://localhost/admin/toIndex.html
3.  后台默认账号密码：lyscms   123456

